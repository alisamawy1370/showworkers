package com.wakecap.show_worker.injection.scope

import javax.inject.Scope

/**
 * Created by ali_samawi on 29/8/17.
 */
@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class PerActivity