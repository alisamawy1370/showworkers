package com.wakecap.show_worker.injection.module

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * module to represent context for all needed provides
 *
 * @author ali_samawi
 */
@Module
class AppContextModule(val context: Context) {

    @Provides
    fun provideContext(): Context = context
}