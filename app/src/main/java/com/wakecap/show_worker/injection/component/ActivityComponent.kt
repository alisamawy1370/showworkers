package com.wakecap.show_worker.injection.component

import com.wakecap.show_worker.view.main_kotlin.MainActivityK
import com.wakecap.show_worker.injection.scope.PerActivity
import com.wakecap.show_worker.view.main_java.MainActivityJ
import com.wakecap.show_worker.view.main_kotlin.fragment.MainFragmentK
import com.wakecap.show_worker.view.menu.MenuActivity
import dagger.Subcomponent

/**
 * Created by ali_samawi on 29/8/17.
 */
@PerActivity
@Subcomponent
interface ActivityComponent {
    fun inject(activity: MenuActivity)
    fun inject(activity: MainActivityK)
    fun inject(activity: MainActivityJ)
    fun inject(fragment: MainFragmentK)

}