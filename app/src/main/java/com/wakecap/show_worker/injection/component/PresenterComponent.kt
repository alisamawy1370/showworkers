package com.wakecap.show_worker.injection.component

import com.wakecap.show_worker.injection.scope.PerPresenter

import dagger.Subcomponent

/**
 * Created by ali_samawi on 29/11/17.
 */
@PerPresenter
@Subcomponent
interface PresenterComponent {

}