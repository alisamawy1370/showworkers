package com.wakecap.show_worker.injection.qualifier

import javax.inject.Qualifier


@Qualifier
@Retention(value=AnnotationRetention.RUNTIME)
annotation class WithoutToken