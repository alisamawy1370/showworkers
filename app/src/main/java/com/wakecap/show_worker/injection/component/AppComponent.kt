package com.wakecap.show_worker.injection.component

import com.wakecap.show_worker.injection.module.AppContextModule
import com.wakecap.show_worker.injection.module.NetworkModule
import dagger.Component
import javax.inject.Singleton


/**
 * Created by ali_samawi on 29/8/17.
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class ,AppContextModule::class))
interface AppComponent {

    fun activityComponent(): ActivityComponent
    fun presenterComponent(): PresenterComponent

}