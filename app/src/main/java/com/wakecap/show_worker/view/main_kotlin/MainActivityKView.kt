package com.wakecap.show_worker.view.main_kotlin

import com.wakecap.show_worker.data.model.Worker
import com.wakecap.show_worker.view.base.MvpView
import java.util.ArrayList

interface MainActivityKView : MvpView{
    fun onGetWorkersResponse(workers: ArrayList<Worker>)
    fun onGetErrorWorkersResponse(error: String)
}