package com.wakecap.show_worker.view.main_kotlin

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.wakecap.show_worker.R
import com.wakecap.show_worker.data.model.Worker
import com.wakecap.show_worker.view.base.BaseActivity
import com.wakecap.show_worker.view.base.BasePresenter
import kotlinx.android.synthetic.main.activity_main_kotlin.*
import java.util.ArrayList
import javax.inject.Inject
import android.support.design.widget.TabLayout



class MainActivityK : BaseActivity(), MainActivityKView {

    @Inject
    lateinit var presenter: MainActivityKPresenter

    private var adapter: ViewPagerAdapter? = null

    override fun getLayoutResId(): Int = R.layout.activity_main_kotlin

    override fun getPresenter(): BasePresenter<*>? = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getAppComponent().activityComponent().inject(this)
        presenter.bind(this, getAppComponent())

        setupViews()
        loadData()
    }

    fun loadData(){
        presenter.loadData()
    }

    private fun setupViews() {
        title = resources.getString(R.string.show_workers_by_roles)
        adapter = ViewPagerAdapter(supportFragmentManager, 0)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 3

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        btnRefresh.setOnClickListener { loadData() }
    }

    override fun showProgressDialog() {
        progressBar.visibility = View.VISIBLE
        btnRefresh.visibility = View.GONE
    }

    override fun dismissProgressDialog() {
        progressBar.visibility = View.GONE
    }

    override fun onGetWorkersResponse(workers: ArrayList<Worker>) {
        btnRefresh.visibility = View.GONE

        var workerHashList: List<Pair<Int, List<Worker>>>
        workers.groupBy {
            it.attributes.Role.id
        }.apply {
            workerHashList = this.toList()
        }

        tabLayout.removeAllTabs()
        workerHashList.forEach {
            val tabName = if ((it.second[0].attributes.Role.attributes.name).isNotEmpty()){
                it.second[0].attributes.Role.attributes.name
            } else {
                "Role " + it.first
            }
            tabLayout.addTab(tabLayout.newTab().setText(tabName))
        }

        adapter?.numberOfPages = workerHashList.size
        adapter?.workerHashList = workerHashList
        adapter?.notifyDataSetChanged()

    }

    override fun onGetErrorWorkersResponse(error: String) {
        btnRefresh.visibility = View.VISIBLE

        Snackbar.make(mainHolder, error, Snackbar.LENGTH_SHORT).show()
    }
}
