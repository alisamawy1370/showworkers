package com.wakecap.show_worker.view.main_kotlin.fragment

import com.wakecap.show_worker.view.base.BasePresenter
import javax.inject.Inject

class MainFragmentPresenter @Inject constructor() : BasePresenter<MainFragmentView>(){
}