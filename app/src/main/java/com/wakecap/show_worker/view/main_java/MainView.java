package com.wakecap.show_worker.view.main_java;

import com.wakecap.show_worker.data.model.Worker;
import com.wakecap.show_worker.view.base.MvpView;

import java.util.ArrayList;

public interface MainView extends MvpView {
    void onGetWorkersResponse(ArrayList<Worker> workers);
    void onGetErrorWorkersResponse(String error);
}
