package com.wakecap.show_worker.view.main_java;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.wakecap.show_worker.R;
import com.wakecap.show_worker.data.model.Worker;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;

public class WorkerAdapter extends RecyclerView.Adapter<WorkerAdapter.WorkerViewHolder> {

    ArrayList<Worker> workerList;

    public WorkerAdapter(ArrayList<Worker> workerList) {
        this.workerList = workerList;
    }


    @NonNull
    @Override
    public WorkerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_worker_j, parent, false);
        return new WorkerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkerViewHolder workerViewHolder, int i) {
        workerViewHolder.bind(workerList.get(i));
    }

    @Override
    public int getItemCount() {
        return workerList.size();
    }

    class WorkerViewHolder extends RecyclerView.ViewHolder {

        TextView tvId = itemView.findViewById(R.id.tvId);
        TextView tvName = itemView.findViewById(R.id.tvName);
        TextView tvRoleValue = itemView.findViewById(R.id.tvRoleValue);
        TextView tvContractorValue = itemView.findViewById(R.id.tvContractorValue);
        CircleImageView imgHelmetColor = itemView.findViewById(R.id.imgHelmetColor);

        WorkerViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(Worker worker) {
            tvId.setText( "#" + String.valueOf(worker.getId()));
            tvName.setText(worker.getAttributes().getFull_name());
            tvRoleValue.setText(worker.getAttributes().getMain_role());

            if (worker.getAttributes().getContractor() != null) {
                tvContractorValue.setText(worker.getAttributes().getContractor());
            } else {
                tvContractorValue.setText(itemView.getResources().getString(R.string.not_available));
            }

            if (worker.getAttributes().getHelmet_color() != null) {
                int color = Integer.parseInt(worker.getAttributes().getHelmet_color().replaceFirst("^#",""), 16);
                if (color != 0)
                    imgHelmetColor.setColorFilter(color);
                else
                    imgHelmetColor.setColorFilter(itemView.getResources().getColor(R.color.dark_black));
            } else {
                    imgHelmetColor.setColorFilter(itemView.getResources().getColor(R.color.dark_black));
            }
        }
    }

}
