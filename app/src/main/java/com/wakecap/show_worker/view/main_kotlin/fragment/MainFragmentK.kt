package com.wakecap.show_worker.view.main_kotlin.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wakecap.show_worker.R
import com.wakecap.show_worker.data.model.Worker
import com.wakecap.show_worker.view.base.BaseFragment
import com.wakecap.show_worker.view.base.BasePresenter
import java.util.ArrayList
import javax.inject.Inject
import kotlinx.android.synthetic.main.fragment_main_kotlin.*

class MainFragmentK : BaseFragment() , MainFragmentView {

    @Inject lateinit var presenter: MainFragmentPresenter

    override fun getPresenter(): BasePresenter<*>? = presenter

    private var mAdapter : WorkerAdapterK? = null

    companion object {
        val DATA_LIST = "DATA_LIST"

        fun newInstance(workers: ArrayList<Worker> = arrayListOf()): MainFragmentK {
            val myFragment = MainFragmentK()

            val args = Bundle()
            args.putSerializable( DATA_LIST , workers)
            myFragment.arguments = args

            return myFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getAppComponent().activityComponent().inject(this)
        presenter.bind(this, getAppComponent())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(
            R.layout.fragment_main_kotlin,
            container, false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews(){
        val workers = if (arguments?.getSerializable(DATA_LIST) != null){
            arguments?.getSerializable(DATA_LIST) as ArrayList<Worker>
        } else {
            arrayListOf()
        }

        mAdapter = WorkerAdapterK(workers)
        val mLayoutManager = LinearLayoutManager(context)
        rvWorkers.layoutManager = mLayoutManager
        rvWorkers.adapter = mAdapter
    }
}