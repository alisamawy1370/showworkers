package com.wakecap.show_worker.view.menu

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.wakecap.show_worker.R
import com.wakecap.show_worker.view.base.BaseActivity
import com.wakecap.show_worker.view.base.BasePresenter
import com.wakecap.show_worker.view.main_java.MainActivityJ
import com.wakecap.show_worker.view.main_kotlin.MainActivityK
import kotlinx.android.synthetic.main.activity_menu.*
import javax.inject.Inject

class MenuActivity : BaseActivity() , MenuView{

    @Inject
    lateinit var presenter: MenuPresenter

    override fun getLayoutResId(): Int = R.layout.activity_menu

    override fun getPresenter(): BasePresenter<*>? = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState)

        getAppComponent().activityComponent().inject(this)
        presenter.bind(this, getAppComponent())

        setupViews()

    }

    private fun setupViews(){
        btnShowTotalWorker.setOnClickListener {
            startActivity(Intent(this , MainActivityJ::class.java))
        }

        btnCategorizeWorker.setOnClickListener {
            startActivity(Intent(this , MainActivityK::class.java))
        }
    }
}