package com.wakecap.show_worker.view.main_kotlin.fragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wakecap.show_worker.R
import com.wakecap.show_worker.data.model.Worker
import de.hdodenhof.circleimageview.CircleImageView
import java.util.ArrayList


class WorkerAdapterK(var workerList: ArrayList<Worker>) :
    RecyclerView.Adapter<WorkerAdapterK.WorkerViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, i: Int): WorkerViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_worker_j, parent, false)
        return WorkerViewHolder(itemView)
    }

    override fun onBindViewHolder(workerViewHolder: WorkerViewHolder, i: Int) {
        workerViewHolder.bind(workerList[i])
    }

    override fun getItemCount(): Int {
        return workerList.size
    }

    inner class WorkerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvId = itemView.findViewById<TextView>(R.id.tvId)
        var tvName = itemView.findViewById<TextView>(R.id.tvName)
        var tvRoleValue = itemView.findViewById<TextView>(R.id.tvRoleValue)
        var tvContractorValue = itemView.findViewById<TextView>(R.id.tvContractorValue)
        var imgHelmetColor = itemView.findViewById<CircleImageView>(R.id.imgHelmetColor)

        fun bind(worker: Worker) {
            tvId.text = "#" + worker.id.toString()
            tvName.text = worker.attributes.full_name
            tvRoleValue.text = worker.attributes.main_role

            if (worker.attributes.contractor != null) {
                tvContractorValue.text = worker.attributes.contractor
            } else {
                tvContractorValue.text = itemView.resources.getString(R.string.not_available)
            }

            if (worker.attributes.helmet_color != null) {
                val color = Integer.parseInt(worker.attributes.helmet_color.replaceFirst("^#".toRegex(), ""), 16)
                if (color != 0)
                    imgHelmetColor.setColorFilter(color)
                else
                    imgHelmetColor.setColorFilter(itemView.resources.getColor(R.color.dark_black))
            } else {
                imgHelmetColor.setColorFilter(itemView.resources.getColor(R.color.dark_black))
            }
        }
    }

}
