package com.wakecap.show_worker.view.base

import com.afollestad.materialdialogs.MaterialDialog
import com.wakecap.show_worker.data.ErrorModel

/**
 * Created by ali on 12/13/18.
 */
interface MvpView {
    fun showProgressDialog() {}
    fun dismissProgressDialog() {}
    fun showError(error: ErrorModel, showDialog: Boolean = true)
    fun showDialogWithRetry(title: String?, content: String?, retryTitle: String, okTitle: String, onOkClick: (MaterialDialog) -> Unit, onRetryClick: (MaterialDialog) -> Unit) {}
    fun showDialogWithRetryStringsID(title: Int, content: String?, retryTitle: Int, okTitle: Int, onOkClick: (MaterialDialog) -> Unit, onRetryClick: (MaterialDialog) -> Unit) {}
    fun onTokenExpired() {}

}