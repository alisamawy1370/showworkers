package com.wakecap.show_worker.view.main_kotlin


import com.wakecap.show_worker.data.model.WorkersResponse
import com.wakecap.show_worker.usecase.GetWorkersInfoUseCase
import com.wakecap.show_worker.view.base.BasePresenter
import javax.inject.Inject

class MainActivityKPresenter @Inject constructor(private val getWorkersInfoUseCase: GetWorkersInfoUseCase) :
    BasePresenter<MainActivityKView>() {

    fun loadData() {
        view?.showProgressDialog()
        getWorkersInfoUseCase.observe(
            { workersResponse: WorkersResponse ->
                view?.onGetWorkersResponse(workersResponse.getWorker())
                view?.dismissProgressDialog()
            },
            { (_, _, errorMessage) ->
                view?.onGetErrorWorkersResponse(errorMessage)
                view?.dismissProgressDialog()
                Unit
            }).register(this)

    }
}