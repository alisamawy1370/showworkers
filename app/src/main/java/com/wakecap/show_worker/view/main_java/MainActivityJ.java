package com.wakecap.show_worker.view.main_java;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import com.wakecap.show_worker.R;
import com.wakecap.show_worker.data.model.Worker;
import com.wakecap.show_worker.view.base.BaseActivity;
import com.wakecap.show_worker.view.base.BasePresenter;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import java.util.ArrayList;

public class MainActivityJ extends BaseActivity implements MainView {

    private ConstraintLayout mainHolder;
    private RecyclerView rvRecycler;
    private SwipeRefreshLayout swipeRefresh;
    private ImageView refreshView;
    private WorkerAdapter mAdapter;

    @Inject
    MainPresenterJ presenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main_java;
    }

    @Nullable
    @Override
    protected BasePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getAppComponent(this).activityComponent().inject(this);
        presenter.bind(this, getAppComponent(this));

        setupViews();
        loadData();
    }


    private void setupViews() {
        setTitle(getResources().getString(R.string.show_all_workers));
        mainHolder = findViewById(R.id.mainHolder);
        rvRecycler = findViewById(R.id.rvWorkers);
        swipeRefresh = findViewById(R.id.swipeRefresh);
        refreshView = findViewById(R.id.btnRefresh);

        mAdapter = new WorkerAdapter(new ArrayList<>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecycler.setLayoutManager(mLayoutManager);
        rvRecycler.setAdapter(mAdapter);


        refreshView.setOnClickListener(view -> {
            loadData();
        });

        swipeRefresh.setOnRefreshListener(this::loadData);
    }

    private void loadData() {
        presenter.loadData();
    }


    @Override
    public void showProgressDialog() {
        swipeRefresh.setRefreshing(true);
        refreshView.setVisibility(View.GONE);
    }

    @Override
    public void dismissProgressDialog() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onGetWorkersResponse(ArrayList<Worker> workers) {
        refreshView.setVisibility(View.GONE);
        mAdapter.workerList = workers;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGetErrorWorkersResponse(String error) {
        if (mAdapter.workerList.isEmpty())
            refreshView.setVisibility(View.VISIBLE);

        Snackbar.make(mainHolder, error, Snackbar.LENGTH_SHORT).show();
    }
}
