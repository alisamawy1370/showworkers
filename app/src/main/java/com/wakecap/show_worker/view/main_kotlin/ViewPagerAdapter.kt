package com.wakecap.show_worker.view.main_kotlin

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.wakecap.show_worker.data.model.Worker
import com.wakecap.show_worker.view.main_kotlin.fragment.MainFragmentK

class ViewPagerAdapter( fm : FragmentManager , var numberOfPages : Int)
    : FragmentStatePagerAdapter(fm) {

    private var fragment: Fragment? = null
    var workerHashList: List<Pair<Int, List<Worker>>> = listOf()

    override fun getItem(position: Int): Fragment? {
        for (i in 0 until numberOfPages) {
            if (i == position) {
                fragment = MainFragmentK.newInstance(ArrayList(workerHashList[position].second))
                break
            }
        }
        return fragment
    }

    override fun getCount(): Int = numberOfPages
}