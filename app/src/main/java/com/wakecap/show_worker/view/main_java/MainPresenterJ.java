package com.wakecap.show_worker.view.main_java;

import com.wakecap.show_worker.data.ErrorModel;
import com.wakecap.show_worker.data.model.WorkersResponse;
import com.wakecap.show_worker.usecase.GetWorkersInfoUseCase;
import com.wakecap.show_worker.view.base.BasePresenter;
import kotlin.Unit;
import javax.inject.Inject;

class MainPresenterJ extends BasePresenter<MainView> {

    private GetWorkersInfoUseCase getWorkersInfoUseCase ;

    @Inject
    MainPresenterJ(GetWorkersInfoUseCase getWorkersInfoUseCase){
        this.getWorkersInfoUseCase = getWorkersInfoUseCase;
    }

    void loadData(){
        getView().showProgressDialog();
        getWorkersInfoUseCase.observe(
                (WorkersResponse workersResponse) -> {
                    getView().onGetWorkersResponse(workersResponse.getWorker());
                    getView().dismissProgressDialog();
                    return Unit.INSTANCE;
                    } ,
                (ErrorModel e)-> {
                    getView().onGetErrorWorkersResponse(e.getErrorMessage());
                    getView().dismissProgressDialog();
                    return Unit.INSTANCE;
                }).register(this);

    }
}
