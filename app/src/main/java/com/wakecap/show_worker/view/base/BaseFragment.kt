package com.wakecap.show_worker.view.base

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.wakecap.show_worker.injection.component.ActivityComponent
import com.wakecap.show_worker.R
import io.reactivex.disposables.CompositeDisposable
import com.wakecap.show_worker.data.ErrorModel
import com.wakecap.show_worker.injection.component.AppComponent
import com.wakecap.show_worker.MyApplication


/**
 * Created by ali_samawi on 30/8/17.
 */
abstract class BaseFragment : Fragment(), MvpView
{
    protected var activityComponent: ActivityComponent? = null
    protected val disposables = CompositeDisposable()

    private var baseUtils: BaseUtils? = null

    protected abstract fun getPresenter(): BasePresenter<*>?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        this.baseUtils = BaseUtils(this)
        activityComponent = context!!.getAppComponent().activityComponent()

    }

    override fun onStop() {
        baseUtils?.dismissLoadingProgressDialog()
        super.onStop()
    }

    override fun onDestroy() {
        activityComponent = null
        getPresenter()?.destroy()
        disposables.dispose()
        super.onDestroy()
    }

    override fun showProgressDialog() {
        baseUtils?.showLoadingProgressDialog(getString(R.string.please_wait), getString(R.string.connecting))
    }

    override fun dismissProgressDialog() {
        baseUtils?.dismissLoadingProgressDialog()
    }

    override fun onTokenExpired() {
        baseUtils?.onTokenExpired()
    }

    override fun showDialogWithRetry(title: String?, content: String?, retryTitle: String, okTitle: String, onOkClick: (MaterialDialog) -> Unit, onRetryClick: (MaterialDialog) -> Unit) {
        baseUtils?.showDialogWithRetry(title ?: "", content ?: "", retryTitle, okTitle, onOkClick, onRetryClick)
    }

    override fun showDialogWithRetryStringsID(title: Int, content: String?, retryTitle: Int, okTitle: Int, onOkClick: (MaterialDialog) -> Unit, onRetryClick: (MaterialDialog) -> Unit) {
        baseUtils?.showDialogWithRetryByIds(title, content ?: "", retryTitle, okTitle, onOkClick, onRetryClick)
    }

    override fun showError(error: ErrorModel, showDialog: Boolean) {
        activity?.let {
            baseUtils?.showError(error,activity!!.supportFragmentManager,showDialog)
        }
    }

    fun Context.getAppComponent(): AppComponent = (applicationContext as MyApplication).appComponent

    fun getAppComponent() = context!!.getAppComponent()
}