package com.wakecap.show_worker

import android.app.Application
import android.util.Log
import com.wakecap.show_worker.injection.component.AppComponent
import com.wakecap.show_worker.injection.component.DaggerAppComponent
import com.wakecap.show_worker.injection.module.AppContextModule
import java.text.SimpleDateFormat
import java.util.*

class MyApplication : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        setupComponent()

    }

    private fun setupComponent() {
        appComponent = DaggerAppComponent.builder()
                .appContextModule(AppContextModule(applicationContext))
                .build()
    }
}

fun <T : Any> T.log(tag: String = "taggg", isError: Boolean = false, isCurrentTimeNeedded: Boolean = false, string: String = ""): T {
    var newTag = tag
    if (isCurrentTimeNeedded) {
        val currentTimeStr = SimpleDateFormat("mm:ss.SSS", Locale.US).format(Calendar.getInstance().time).toString()
        newTag = "$tag ( $currentTimeStr )"
    }

    when (this) {
        is List<*> -> this.forEach {
            if (!isError)
                Log.d(newTag, it.toString())
            else
                Log.e(newTag, it.toString())
        }
        else -> {
            if (!isError)
                Log.d(newTag, this.toString())
            else
                Log.e(newTag, this.toString())
        }
    }
    return this
}
