package com.wakecap.show_worker.data.repository

import com.wakecap.show_worker.data.model.WorkersResponse
import io.reactivex.Single
import com.wakecap.show_worker.data.restful.APIs
import com.wakecap.show_worker.data.restful.APIsWithToken
import com.wakecap.show_worker.injection.qualifier.WithToken
import com.wakecap.show_worker.injection.qualifier.WithoutToken
import javax.inject.Inject

/**
 * main repository that is responsible for all data connections with server
 * @author ali_samawi
 */
class CloudRepository @Inject constructor(
        @WithToken private val apIsWithToken: APIsWithToken,
        @WithoutToken private val api: APIs
) : CloudRepositoryContract {

    override fun getWorkersInfo(): Single<WorkersResponse>
            = apIsWithToken.getWorkersInfo()

}
