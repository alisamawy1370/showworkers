package com.wakecap.show_worker.data

/**
 * Created by ali_samawi on 22/10/17.
 */
data class ErrorModel(
        val e: Exception?,
        val errorCode: Int,
        val errorMessage: String,
        val isConnectionError: Boolean
)