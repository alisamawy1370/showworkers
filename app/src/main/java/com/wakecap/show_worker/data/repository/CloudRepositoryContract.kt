package com.wakecap.show_worker.data.repository

import com.wakecap.show_worker.data.model.WorkersResponse
import io.reactivex.Single


/**
 * Created by ali_samawi on 29/11/17.
 */
interface CloudRepositoryContract {
    fun getWorkersInfo(): Single<WorkersResponse>

}