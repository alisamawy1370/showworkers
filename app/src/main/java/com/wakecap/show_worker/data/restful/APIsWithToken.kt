package com.wakecap.show_worker.data.restful

import com.wakecap.show_worker.data.model.WorkersResponse
import io.reactivex.Single
import retrofit2.http.GET


/**
 * Created by ali_samawi on 29/8/17.
 */
interface APIsWithToken {

    @GET("/api/sites/10010001/workers")
    fun getWorkersInfo(): Single<WorkersResponse>
}