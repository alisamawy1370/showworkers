package com.wakecap.show_worker.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

data class Worker(@SerializedName("id") val id : Int,
                  @SerializedName("type") val type : String,
                  @SerializedName("attributes") val attributes : Attribute
):Serializable

data class Attribute(@SerializedName("helmet_color") val helmet_color : String,
                     @SerializedName("site_id") val site_id : Int,
                     @SerializedName("role_id") val role_id : String,
                     @SerializedName("first_name") val first_name : String,
                     @SerializedName("last_name") val last_name : String,
                     @SerializedName("worker_tag_id") val worker_tag_id : String?,
                     @SerializedName("contractor") val contractor : String?,
                     @SerializedName("mobile_number") val mobile_numbera : String?,
                     @SerializedName("created_at") val created_at : Date?,
                     @SerializedName("updated_at") val updated_at : String?,
                     @SerializedName("deleted_at") val deleted_at : String?,
                     @SerializedName("role") val main_role : String,
                     @SerializedName("full_name") val full_name : String,
                     @SerializedName("Inventories") val Inventories : ArrayList<Inventory>,
                     @SerializedName("Role") val Role : Role
                     ):Serializable

data class Inventory(@SerializedName("id") val id : Int,
                     @SerializedName("type") val type : String,
                     @SerializedName("attributes") val attributes : InventoryAttribute):Serializable

data class InventoryAttribute(@SerializedName("site_id") val site_id : Int,
                     @SerializedName("id") val id : Int,
                     @SerializedName("name") val name : String,
                     @SerializedName("type") val type : String,
                     @SerializedName("maximum_voltage") val maximum_voltage : Int,
                     @SerializedName("is_active") val is_active : Boolean,
                     @SerializedName("last_seen") val last_seen : Date?,
                     @SerializedName("created_at") val created_at : Date?,
                     @SerializedName("updated_at") val updated_at : Date?,
                     @SerializedName("deleted_at") val deleted_at : Date?,
                     @SerializedName("is_online") val is_online : Boolean
                     ):Serializable

data class Role(@SerializedName("id") val id : Int,
                     @SerializedName("type") val type : String,
                     @SerializedName("attributes") val attributes : RolAttribute):Serializable

data class RolAttribute(@SerializedName("id") val id : Int,
                     @SerializedName("name") val name : String,
                     @SerializedName("created_at") val created_at : Date?,
                     @SerializedName("updated_at") val updated_at : Date?,
                     @SerializedName("deleted_at") val deleted_at : Date?
                     ):Serializable

