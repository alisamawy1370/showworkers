package com.wakecap.show_worker.data.model

import com.google.gson.annotations.SerializedName

data class WorkersResponse(@SerializedName("data") val data : Item ,
                           @SerializedName("message") val message : String){
    fun getWorker() = data.workers
}


data class Pagination(@SerializedName("total") val total : Int ,
                           @SerializedName("per_page") val per_page : Int,
                           @SerializedName("current_page") val current_page : Int,
                           @SerializedName("last_page") val last_page : Int,
                           @SerializedName("next") val next : Int,
                           @SerializedName("previous") val per_previouspage : Int
)

data class Item(@SerializedName("items") val workers : ArrayList<Worker> ,
                @SerializedName("meta") val pagination : Pagination)