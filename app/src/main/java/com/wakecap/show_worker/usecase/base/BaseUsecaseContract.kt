package com.wakecap.show_worker.usecase.base

import com.wakecap.show_worker.data.ErrorModel


/**
 * Created by ali_samawi on 3/21/2017.
 */
interface BaseUsecaseContract<T> {
    val isCompleted: Boolean
    val isInProgress: Boolean
    val isFailed: Boolean
    fun cancel()
    fun observe(onSuccess: (T) -> Unit,
                onError: (e: ErrorModel) -> Unit): BaseUsecase<T>
}
