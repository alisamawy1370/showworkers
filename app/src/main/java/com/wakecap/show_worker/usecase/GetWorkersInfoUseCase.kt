package com.wakecap.show_worker.usecase

import com.wakecap.show_worker.data.model.WorkersResponse
import com.wakecap.show_worker.data.repository.CloudRepository
import com.wakecap.show_worker.usecase.base.BaseUsecase
import io.reactivex.Single
import javax.inject.Inject

class GetWorkersInfoUseCase @Inject constructor(private val cloudRepository: CloudRepository) : BaseUsecase<WorkersResponse>() {

    override fun createObservable(): Single<WorkersResponse> {
        return cloudRepository.getWorkersInfo()
    }
}